﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewXMLEditor
{
    public partial class AddCat : Form
    {
        public string catName = String.Empty;

        public AddCat()
        {
            InitializeComponent();
        }

        public AddCat(string str)
        {
            InitializeComponent();
            textBox1.Text = str;
        }

        // cancel
        private void btnCancel_Click(object sender, EventArgs e)
        {
            catName = "";
            this.Hide();
        }

        // confirm
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text == "")
                    throw new CException("EmptyText");

                catName = textBox1.Text;
                this.Hide();
            }
            catch (Exception)
            {

            }       
        }

        public class CException : Exception
        {
            public CException(string s)
            {
                if (s == "EmptyText")
                {
                    MessageBox.Show("Please fill up all the blank!");
                }
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {

            if (keyData == Keys.Escape)
            {
                catName = "";
                this.Hide();
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}