﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
      Copyright {2016} {Wong Yan Yin, Jackson Teh Ka Sing}

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
*/


namespace NewXMLEditor
{
    public class TestCase
    {
        private string tcNo;
        private string desc;
        public List<TestSequence> seq;

        public TestCase()
        {
            this.tcNo = string.Empty;
            this.desc = string.Empty;
            seq = new List<TestSequence>();
        }

        public TestCase(string tcNo, string desc)
        {
            setTcNo(tcNo);
            setDesc(desc);
            seq = new List<TestSequence>();
        }

        public string getTcNo() { return tcNo; }
        public string getDesc() { return desc; }

        public void setTcNo(string tc) { tcNo = tc; }
        public void setDesc(string d) { desc = d; }
    }
}
