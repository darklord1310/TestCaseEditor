﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
      Copyright {2016} {Wong Yan Yin, Jackson Teh Ka Sing}

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
*/


namespace NewXMLEditor
{
    public class TestSequence
    {
        private string sNo;
        private string description;
        private string diagCmd;
        private string para;
        private string expected;

        public TestSequence()
        {
            this.sNo = string.Empty;
            this.description = string.Empty;
            this.diagCmd = string.Empty;
            this.para = string.Empty;
            this.expected = string.Empty;
        }

        public TestSequence(string no, string desc, string cmd, string para, string expected)
        {
            setSeqNo(no);
            setDesc(desc);
            setDiagCmd(cmd);
            setPara(para);
            setExpected(expected);
        }

        public string getSeqNo() { return sNo; }
        public string getDesc() { return description; }
        public string getDiagCmd() { return diagCmd; }
        public string getPara() { return para; }
        public string getExpected() { return expected; }

        public void setSeqNo(string no) { sNo = no; }
        public void setDesc(string desc) { description = desc; }
        public void setDiagCmd(string cmd) { this.diagCmd = cmd; }
        public void setPara(string para) { this.para = para; }
        public void setExpected(string exp) { expected = exp; }
    }
}
