﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewXMLEditor
{
    public class Module
    {
        private string category;
        private string moduleName;
        public List<TestCase> tc;

        public Module()
        {
            this.category = string.Empty;
            this.moduleName = string.Empty;
            tc = new List<TestCase>();
        }

        public Module(string cg, string mod)
        {
            setCategory(cg);
            setModuleName(mod);
            tc = new List<TestCase>();
        }

        public string getCategory() { return category; }
        public string getModuleName() { return moduleName; }

        public void setCategory(string category) { this.category = category; }
        public void setModuleName(string mod) { moduleName = mod; }
    }
}
