﻿namespace NewXMLEditor
{
    /*
      Copyright {2016} {Wong Yan Yin, Jackson Teh Ka Sing}

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
    */

    partial class xmlEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(xmlEditor));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.TreeviewIL = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.createNewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openSingleXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openXMLsInFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tcCancel = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gBoxSeq = new System.Windows.Forms.GroupBox();
            this.SeqDataGridView = new System.Windows.Forms.DataGridView();
            this.SeqNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiagCmd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Para = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Exp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tcSave = new System.Windows.Forms.Button();
            this.testDesc = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.testNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.tcCancel.SuspendLayout();
            this.gBoxSeq.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SeqDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.AllowDrop = true;
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.treeView1.Location = new System.Drawing.Point(0, 24);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(199, 600);
            this.treeView1.TabIndex = 0;
            this.treeView1.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.treeView1_ItemDrag);
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            this.treeView1.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeView1_DragDrop);
            this.treeView1.DragEnter += new System.Windows.Forms.DragEventHandler(this.treeView1_DragEnter);
            this.treeView1.DragOver += new System.Windows.Forms.DragEventHandler(this.treeView1_DragOver);
            this.treeView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseUp);
            // 
            // TreeviewIL
            // 
            this.TreeviewIL.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("TreeviewIL.ImageStream")));
            this.TreeviewIL.TransparentColor = System.Drawing.Color.Transparent;
            this.TreeviewIL.Images.SetKeyName(0, "node.png");
            this.TreeviewIL.Images.SetKeyName(1, "add.png");
            this.TreeviewIL.Images.SetKeyName(2, "delete.png");
            this.TreeviewIL.Images.SetKeyName(3, "edit.png");
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.DarkTurquoise;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createNewToolStripMenuItem,
            this.openToolStripMenuItem,
            this.exportToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // createNewToolStripMenuItem
            // 
            this.createNewToolStripMenuItem.AutoToolTip = true;
            this.createNewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("createNewToolStripMenuItem.Image")));
            this.createNewToolStripMenuItem.Name = "createNewToolStripMenuItem";
            this.createNewToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.createNewToolStripMenuItem.Text = "Create";
            this.createNewToolStripMenuItem.ToolTipText = "Create a new test case from scratch";
            this.createNewToolStripMenuItem.Click += new System.EventHandler(this.createNewToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.AutoToolTip = true;
            this.openToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openSingleXMLToolStripMenuItem,
            this.openXMLsInFolderToolStripMenuItem});
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.ToolTipText = "Open existing test case and allow modifications";
            // 
            // openSingleXMLToolStripMenuItem
            // 
            this.openSingleXMLToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openSingleXMLToolStripMenuItem.Image")));
            this.openSingleXMLToolStripMenuItem.Name = "openSingleXMLToolStripMenuItem";
            this.openSingleXMLToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.openSingleXMLToolStripMenuItem.Text = "Open single XML";
            this.openSingleXMLToolStripMenuItem.Click += new System.EventHandler(this.openSingleXMLToolStripMenuItem_Click);
            // 
            // openXMLsInFolderToolStripMenuItem
            // 
            this.openXMLsInFolderToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openXMLsInFolderToolStripMenuItem.Image")));
            this.openXMLsInFolderToolStripMenuItem.Name = "openXMLsInFolderToolStripMenuItem";
            this.openXMLsInFolderToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.openXMLsInFolderToolStripMenuItem.Text = "Open XML(s) in folder";
            this.openXMLsInFolderToolStripMenuItem.Click += new System.EventHandler(this.openXMLsInFolderToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.AutoToolTip = true;
            this.exportToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exportToolStripMenuItem.Image")));
            this.exportToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.exportToolStripMenuItem.Text = "Export";
            this.exportToolStripMenuItem.ToolTipText = "Export the current state data to XML";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
            // 
            // tcCancel
            // 
            this.tcCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcCancel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tcCancel.Controls.Add(this.btnCancel);
            this.tcCancel.Controls.Add(this.gBoxSeq);
            this.tcCancel.Controls.Add(this.tcSave);
            this.tcCancel.Controls.Add(this.testDesc);
            this.tcCancel.Controls.Add(this.label6);
            this.tcCancel.Controls.Add(this.testNo);
            this.tcCancel.Controls.Add(this.label5);
            this.tcCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcCancel.Location = new System.Drawing.Point(216, 27);
            this.tcCancel.Name = "tcCancel";
            this.tcCancel.Size = new System.Drawing.Size(556, 585);
            this.tcCancel.TabIndex = 2;
            this.tcCancel.TabStop = false;
            this.tcCancel.Text = "Test Case";
            this.tcCancel.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCancel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(467, 545);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(83, 24);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gBoxSeq
            // 
            this.gBoxSeq.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gBoxSeq.Controls.Add(this.SeqDataGridView);
            this.gBoxSeq.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBoxSeq.Location = new System.Drawing.Point(13, 132);
            this.gBoxSeq.Name = "gBoxSeq";
            this.gBoxSeq.Size = new System.Drawing.Size(532, 398);
            this.gBoxSeq.TabIndex = 3;
            this.gBoxSeq.TabStop = false;
            this.gBoxSeq.Text = "Sequences";
            this.gBoxSeq.Visible = false;
            // 
            // SeqDataGridView
            // 
            this.SeqDataGridView.AllowUserToResizeColumns = false;
            this.SeqDataGridView.AllowUserToResizeRows = false;
            this.SeqDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SeqDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SeqDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedVertical;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.SeqDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.SeqDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.SeqDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SeqNo,
            this.Desc,
            this.DiagCmd,
            this.Para,
            this.Exp});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SeqDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.SeqDataGridView.EnableHeadersVisualStyles = false;
            this.SeqDataGridView.Location = new System.Drawing.Point(3, 20);
            this.SeqDataGridView.MultiSelect = false;
            this.SeqDataGridView.Name = "SeqDataGridView";
            this.SeqDataGridView.ReadOnly = true;
            this.SeqDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.SeqDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.SeqDataGridView.Size = new System.Drawing.Size(526, 375);
            this.SeqDataGridView.TabIndex = 3;
            this.SeqDataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SeqDataGridView_CellMouseClick);
            this.SeqDataGridView.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SeqDataGridView_RowHeaderMouseClick);
            this.SeqDataGridView.DragDrop += new System.Windows.Forms.DragEventHandler(this.SeqDataGridView_DragDrop);
            this.SeqDataGridView.DragOver += new System.Windows.Forms.DragEventHandler(this.SeqDataGridView_DragOver);
            this.SeqDataGridView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SeqDataGridView_MouseDown);
            this.SeqDataGridView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SeqDataGridView_MouseMove);
            // 
            // SeqNo
            // 
            this.SeqNo.FillWeight = 76.14214F;
            this.SeqNo.HeaderText = "No.";
            this.SeqNo.Name = "SeqNo";
            this.SeqNo.ReadOnly = true;
            this.SeqNo.Width = 30;
            // 
            // Desc
            // 
            this.Desc.FillWeight = 105.9645F;
            this.Desc.HeaderText = "Description";
            this.Desc.Name = "Desc";
            this.Desc.ReadOnly = true;
            this.Desc.Width = 140;
            // 
            // DiagCmd
            // 
            this.DiagCmd.FillWeight = 105.9645F;
            this.DiagCmd.HeaderText = "Diag Command";
            this.DiagCmd.Name = "DiagCmd";
            this.DiagCmd.ReadOnly = true;
            this.DiagCmd.Width = 85;
            // 
            // Para
            // 
            this.Para.FillWeight = 105.9645F;
            this.Para.HeaderText = "Parameter";
            this.Para.Name = "Para";
            this.Para.ReadOnly = true;
            this.Para.Width = 115;
            // 
            // Exp
            // 
            this.Exp.FillWeight = 105.9645F;
            this.Exp.HeaderText = "Expect";
            this.Exp.Name = "Exp";
            this.Exp.ReadOnly = true;
            this.Exp.Width = 115;
            // 
            // tcSave
            // 
            this.tcSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tcSave.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tcSave.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.tcSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcSave.Location = new System.Drawing.Point(376, 545);
            this.tcSave.Name = "tcSave";
            this.tcSave.Size = new System.Drawing.Size(83, 24);
            this.tcSave.TabIndex = 11;
            this.tcSave.Text = "Save";
            this.tcSave.UseVisualStyleBackColor = false;
            this.tcSave.Visible = false;
            this.tcSave.Click += new System.EventHandler(this.tcSave_Click);
            // 
            // testDesc
            // 
            this.testDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testDesc.Location = new System.Drawing.Point(116, 47);
            this.testDesc.Multiline = true;
            this.testDesc.Name = "testDesc";
            this.testDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.testDesc.Size = new System.Drawing.Size(426, 79);
            this.testDesc.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Description             :";
            // 
            // testNo
            // 
            this.testNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testNo.Location = new System.Drawing.Point(116, 17);
            this.testNo.Name = "testNo";
            this.testNo.Size = new System.Drawing.Size(115, 20);
            this.testNo.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Test Case Number :";
            // 
            // xmlEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CadetBlue;
            this.ClientSize = new System.Drawing.Size(784, 624);
            this.Controls.Add(this.tcCancel);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "xmlEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test Case Editor";
            this.Resize += new System.EventHandler(this.xmlEditor_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tcCancel.ResumeLayout(false);
            this.tcCancel.PerformLayout();
            this.gBoxSeq.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SeqDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.ImageList TreeviewIL;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem createNewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.GroupBox tcCancel;
        private System.Windows.Forms.GroupBox gBoxSeq;
        private System.Windows.Forms.Button tcSave;
        private System.Windows.Forms.TextBox testDesc;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox testNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripMenuItem openSingleXMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openXMLsInFolderToolStripMenuItem;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridView SeqDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn SeqNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiagCmd;
        private System.Windows.Forms.DataGridViewTextBoxColumn Para;
        private System.Windows.Forms.DataGridViewTextBoxColumn Exp;
    }
}

