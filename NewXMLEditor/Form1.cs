﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Diagnostics;
using System.Runtime.CompilerServices;

/*
      Copyright {2016} {Wong Yan Yin, Jackson Teh Ka Sing}

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
*/

namespace NewXMLEditor
{
    public partial class xmlEditor : Form
    {
        enum NodeLevel { TESTMENU, CATEGORY, MODULE, TESTCASE };
        enum SelectionMode { ADD, DELETE, EDIT};
        enum Images { NODE, ADD, DELETE, EDIT };
        int selectionMode;
        ContextMenuStrip docMenu, deleteSeq;
        List<Module> modules = new List<Module>();
        private string NodeMap;
        private const int MAPSIZE = 128;
        private StringBuilder NewNodeMap = new StringBuilder(MAPSIZE);
        string fileDir = string.Empty;
        string appPath = string.Empty, xmlPath = string.Empty;
        int rowToDel;
        bool isTreeViewFreeze = false;

        private Rectangle dragBoxFromMouseDown;
        private int rowIndexFromMouseDown;
        private int rowIndexOfItemUnderMouseToDrop;

        public xmlEditor()
        {
            InitializeComponent();
            this.treeView1.ImageList = TreeviewIL;
            menuStrip1.ShowItemToolTips = true;
            createTreeView();
            createContextMenuStrip();
            tcCancel.Visible = false;
            createXmlFolderPath();
        }

        private void handleGroupBoxShowing(int level)
        {
            if (level == (int)NodeLevel.TESTCASE)
            {
                tcCancel.Visible = true;
                gBoxSeq.Visible = true;
                enableTestCase(selectionMode);
            }
        }

        private void createTreeView()
        {
            treeView1.BeginUpdate();
            TreeNode node = createNormalTreeNode("TestMenu");
            treeView1.Nodes.Add(node);
            treeView1.SelectedNode = node;
            treeView1.EndUpdate();
        }

        public TreeNode createNormalTreeNode(string nodeName)
        {
            TreeNode node = new TreeNode(nodeName);
            node.ImageIndex = (int)Images.NODE;
            node.Name = nodeName;
            return node;
        }

        private void createContextMenuStrip()
        {
            int nodeLevel = treeView1.SelectedNode.Level;
            int nodeCounts = treeView1.SelectedNode.Nodes.Count;
            ToolStripMenuItem deleteLabel, renameLabel, addLabel, deleteLabel2;

            if(docMenu != null)
            {
                docMenu.Dispose();
                deleteSeq.Dispose();
            }

            // Create the ContextMenuStrip.
            docMenu = new ContextMenuStrip();
            deleteSeq = new ContextMenuStrip();

            //Create some menu items.
            deleteLabel = new ToolStripMenuItem();
            deleteLabel.Text = "Delete";
            deleteLabel.Image = TreeviewIL.Images[(int)Images.DELETE];
            renameLabel = new ToolStripMenuItem();
            renameLabel.Text = "Edit";
            renameLabel.Image = TreeviewIL.Images[(int)Images.EDIT];
            addLabel = new ToolStripMenuItem();
            addLabel.Text = "Add";
            addLabel.Image = TreeviewIL.Images[(int)Images.ADD];

            //Add the menu items to the menu.
            if (nodeLevel == (int)NodeLevel.TESTMENU)    //Test Menu
                docMenu.Items.AddRange(new ToolStripMenuItem[] { addLabel });
            else if (nodeLevel == (int)NodeLevel.TESTCASE)
                docMenu.Items.AddRange(new ToolStripMenuItem[] { renameLabel, deleteLabel });
            else
                docMenu.Items.AddRange(new ToolStripMenuItem[] { addLabel, renameLabel, deleteLabel });

            docMenu.ItemClicked += new ToolStripItemClickedEventHandler(contextMenu_ItemClicked);

            deleteLabel2 = new ToolStripMenuItem();
            deleteLabel2.Text = "Delete";
            deleteLabel2.Image = TreeviewIL.Images[(int)Images.DELETE];
            deleteSeq.Items.Add(deleteLabel2);
            deleteSeq.ItemClicked += new ToolStripItemClickedEventHandler(delSeq_ItemClicked);
        }

        static private void reportInvalidIndex(string Message,
                                               [CallerMemberName] string member = "",
                                               [CallerLineNumber] int line = 0)
        {
             MessageBox.Show(Message + " at line " + line + " (" + member + ")");
        }

        private void addNode(TreeNode node)
        {
            if(node.Level == (int)NodeLevel.CATEGORY)
            {
                handleAddModule(node);
            }
            else if (node.Level == (int)NodeLevel.TESTMENU)
            {
                handleAddCategory(node);
            }
            else if(node.Level == (int)NodeLevel.TESTCASE)
            {
                treeView1.Focus();
                //do nothing
            }
            else
            {
                handleAddTestCase(node);
            }
        }

        private void handleAddTestCase(TreeNode node)
        {
            TreeNode dNode = createNormalTreeNode("NoName");
            node.Nodes.Add(dNode);
            treeView1.SelectedNode = dNode;
            handleGroupBoxShowing(treeView1.SelectedNode.Level);
            treeView1.Focus();
            node.Expand();
        }

        private void handleAddModule(TreeNode node)
        {
            TreeNode dNode = createNormalTreeNode("NoName");
            AddModule module = new AddModule();
            module.StartPosition = FormStartPosition.Manual;
            module.Location = new Point(Location.X + (Width - module.Width) / 2, Location.Y + (Height - module.Height) / 2);
            module.ShowDialog(this);         //Make sure we're the owner

            if (module.moduleName != "")
            {
                treeView1.SelectedNode = dNode;
                dNode.Text = module.moduleName;
                dNode.Name = module.moduleName;
                node.Nodes.Add(dNode);
                treeView1.Focus();
                node.Expand();
                modules.Add(new Module(dNode.Parent.Text, dNode.Text));
            }
        }

        private void handleAddCategory(TreeNode node)
        {
            TreeNode dNode = createNormalTreeNode("NoName");
            AddCat cat = new AddCat();
            cat.StartPosition = FormStartPosition.Manual;
            cat.Location = new Point(Location.X + (Width - cat.Width) / 2, Location.Y + (Height - cat.Height) / 2);
            cat.ShowDialog(this);         //Make sure we're the owner

            if (cat.catName != "")
            {
                treeView1.SelectedNode = dNode;
                dNode.Text = cat.catName;
                dNode.Name = cat.catName;
                node.Nodes.Add(dNode);
                treeView1.Focus();
                node.Expand();
            }
        }

        private void deleteNode(TreeNode node)
        {
            TreeNode parent = node.Parent;
            int index;

            if(node.Level == (int)NodeLevel.TESTCASE)
            {
                index = getModuleListIndex(parent.Text);

                if(index == -1)
                {
                    reportInvalidIndex("Invalid index occur");
                    return;
                }

                modules[index].tc.RemoveAt(node.Index);
            }
            else if(node.Level == (int)NodeLevel.TESTMENU)
            {
                showMsgBox("Test Menu root node cannot be deleted!", MessageBoxIcon.Error);
                return;
            }
            else if (node.Level == (int)NodeLevel.CATEGORY)
            {
                docMenu.Hide();
                DialogResult result = MessageBox.Show("Any deleted node cannot be undo. Are you sure?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                    removeCategory(node.Text);
                else return;
            }
            else if (node.Level == (int)NodeLevel.MODULE)
            {
                index = getModuleListIndex(node.Text);

                if (index == -1)
                {
                    reportInvalidIndex("Invalid index occur");
                    return;
                }

                docMenu.Hide();
                DialogResult result = MessageBox.Show("Any deleted node cannot be undo. Are you sure?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                    modules.RemoveAt(index);
                else return;
            }

            node.Remove();
        }

        private void removeCategory(string cat)
        {
            for(int i = 0; i < modules.Count; i++)
            {
                if(string.Equals(cat, modules[i].getCategory()))
                    modules.RemoveAt(i);
            }
        }

        private int getModuleListIndex(string moduleName)
        {
            for (int i = 0; i < modules.Count(); i++ )
            {
                if (string.Equals(modules[i].getModuleName(), moduleName))
                    return i;
            }

            return -1;
        }

        private void editNode(TreeNode node)
        {
            int index;

            if(node.Level == (int)NodeLevel.CATEGORY)
            {
                AddCat cat;
                if (selectionMode != (int)SelectionMode.EDIT)
                    cat = new AddCat();
                else
                    cat = new AddCat(node.Text);

                cat.StartPosition = FormStartPosition.Manual;
                cat.Location = new Point(Location.X + (Width - cat.Width) / 2, Location.Y + (Height - cat.Height) / 2);
                cat.ShowDialog(this);         //Make sure we're the owner

                if (cat.catName != "")
                {
                    setCategory(node.Text, cat.catName);
                    node.Text = cat.catName;
                    node.Name = cat.catName;
                    treeView1.Focus();
                }
            }
            else if(node.Level == (int)NodeLevel.MODULE)
            {
                AddModule mod;
                if (selectionMode != (int)SelectionMode.EDIT)
                    mod = new AddModule();
                else
                    mod = new AddModule(node.Text);

                mod.StartPosition = FormStartPosition.Manual;
                mod.Location = new Point(Location.X + (Width - mod.Width) / 2, Location.Y + (Height - mod.Height) / 2);
                mod.ShowDialog(this);         //Make sure we're the owner

                index = getModuleListIndex(node.Text);

                if (index == -1)
                {
                    reportInvalidIndex("Invalid index occur");
                    return;
                }

                if (mod.moduleName != "")
                {
                    node.Text = mod.moduleName;
                    node.Name = mod.moduleName;
                    modules[index].setModuleName(mod.moduleName);
                    treeView1.Focus();
                }
            }
            else if(node.Level == (int)NodeLevel.TESTCASE)
            {
                enableTestCase(selectionMode);
            }
        }

        private void setCategory(string oldCatName, string newCatName)
        {
            for (int i = 0; i < modules.Count(); i++ )
            {
                if(string.Equals(oldCatName, modules[i].getCategory()))
                    modules[i].setCategory(newCatName);
            }
        }

        private void showMsgBox(string content, MessageBoxIcon iconSelection)
        {
            MessageBox.Show(content, "", MessageBoxButtons.OK, iconSelection);
        }

        void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem item = e.ClickedItem;

            if (item.ToString() == "Edit")
            {
                selectionMode = (int)SelectionMode.EDIT;
                editNode(treeView1.SelectedNode);
            }
            else if (item.ToString() == "Add")
            {
                selectionMode = (int)SelectionMode.ADD;
                addNode(treeView1.SelectedNode);
            }
            else if (item.ToString() == "Delete")
            {
                selectionMode = (int)SelectionMode.DELETE;
                deleteNode(treeView1.SelectedNode);
            }
        }

        void delSeq_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem item = e.ClickedItem;
            int index;
            TreeNode node = treeView1.SelectedNode;
            TreeNode parent = treeView1.SelectedNode.Parent;

            try
            {
                if (item.ToString() == "Delete")
                {
                    SeqDataGridView.Rows.RemoveAt(rowToDel);
                }
            }
            catch(Exception)
            {
                this.showMsgBox("Unable to delete selected row!", MessageBoxIcon.Warning);
            }
        }

        // handle the show context menu event at tree node
        private void treeView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (treeView1.GetNodeAt(e.X, e.Y) != null)
                {
                    treeView1.SelectedNode = treeView1.GetNodeAt(e.X, e.Y);         // get the tree node on mouse right click
                    treeView1.SelectedNode.BackColor = SystemColors.HighlightText;      // highlight the selected node
                    Point p = new Point(treeView1.SelectedNode.Bounds.Right + 1, treeView1.SelectedNode.Bounds.Bottom + 26);
                    docMenu.Show(PointToScreen(p));
                }
            }
        }

        public class CException : Exception
        {
            public CException(string s)
            {
                if (s == "EmptyText")
                    MessageBox.Show("Please fill up all the blank!");
                else if (s == "TooLarge")
                    MessageBox.Show("Value too large for test case number!");
                else if (s == "Duplicate")
                    MessageBox.Show("Duplicate test case found!");
                else if (s == "SequenceError") { }
            }
        }

        private void enableTestCase(int mode)
        {
            testNo.ReadOnly = false;
            isTreeViewFreeze = true;
            testDesc.ReadOnly = false;
            treeView1.Enabled = false;
            testNo.Focus();
            testNo.TabStop = true;
            testDesc.TabStop = true;
            tcSave.Enabled = true;
            tcSave.Visible = true;
            btnCancel.Enabled = true;
            btnCancel.Visible = true;
            SeqDataGridView.ReadOnly = false;
            SeqDataGridView.AllowDrop = true;

            if (mode != (int)SelectionMode.EDIT)
            {
                SeqDataGridView.Rows.Clear();
                testNo.Text = "";
                testDesc.Text = "";
            }
        }

        private void disableTestCase()
        {
            gBoxSeq.Visible = true;
            isTreeViewFreeze = false;
            tcSave.Enabled = false;
            btnCancel.Enabled = false;
            testDesc.ReadOnly = true;
            testNo.ReadOnly = true;
            SeqDataGridView.ReadOnly = true;
            SeqDataGridView.TabStop = false;
            SeqDataGridView.AllowDrop = false;
            testNo.TabStop = false;
            testDesc.TabStop = false;
            treeView1.Enabled = true;
            treeView1.Focus();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if(isTreeViewFreeze == false)
            {
                if (keyData == (Keys.Control | Keys.A))
                {
                    selectionMode = (int)SelectionMode.ADD;
                    if (treeView1.SelectedNode != null)
                        addNode(treeView1.SelectedNode);
                    return true;
                }
                else if (keyData == (Keys.Control | Keys.E))
                {
                    selectionMode = (int)SelectionMode.EDIT;
                    if (treeView1.SelectedNode != null)
                        editNode(treeView1.SelectedNode);
                    return true;
                }
                else if (keyData == (Keys.Control | Keys.D))
                {
                    selectionMode = (int)SelectionMode.DELETE;
                    if (treeView1.SelectedNode != null)
                        deleteNode(treeView1.SelectedNode);
                    return true;
                }
            }
            else
            {
                if(keyData == Keys.Escape)
                {
                    if (selectionMode != (int)SelectionMode.EDIT)
                    {
                        TreeNode node = treeView1.SelectedNode.Parent;
                        treeView1.SelectedNode.Remove();
                        treeView1.SelectedNode = node;
                    }
                    else
                    {
                        testNo.Text = treeView1.SelectedNode.Text.Substring(3, 4).TrimStart('0');
                    }

                    disableTestCase();
                    displaySelectedNode();
                }
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void tcSave_Click(object sender, EventArgs e)
        {
            TreeNode node = treeView1.SelectedNode;
            int index;

            try
            {
                if (testNo.Text == "" || testDesc.Text == "")
                    throw new CException("EmptyText");

                if (Convert.ToInt64(testNo.Text) > 9999)
                    throw new CException("TooLarge");

                node.Text = "TC_" + (Convert.ToInt16(testNo.Text)).ToString("D4");
                node.Name = "TC_" + (Convert.ToInt16(testNo.Text)).ToString("D4");

                index = getModuleListIndex(node.Parent.Text);
                if (index == -1)
                    throw new Exception();

                //if(checkDuplicateTestCase(modules[index].tc, node.Text))
                //    throw new CException("Duplicate");

                if (selectionMode != (int)SelectionMode.EDIT)
                {
                    modules[index].tc.Add(new TestCase(treeView1.SelectedNode.Text, testDesc.Text));
                }
                else
                {
                    modules[index].tc[node.Index].setTcNo(node.Text);
                    modules[index].tc[node.Index].setDesc(testDesc.Text);
                }

                if (!updateSeqNumClass(modules[index].tc[node.Index]))
                    throw new CException("SequenceError");

                disableTestCase();
            }
            catch (FormatException ex)
            {
                showMsgBox("Invalid format for Test Case Number!", MessageBoxIcon.Warning);
            }
            catch (CException) { }
            catch (Exception ex) { reportInvalidIndex("Invalid index occur"); }
        }

        private bool updateSeqNumClass(TestCase tCase)
        {
            tCase.seq.Clear();

            try
            {
                for (int i = 0; i < SeqDataGridView.RowCount - 1; i++)
                {
                    tCase.seq.Add(new TestSequence());

                    if (SeqDataGridView.Rows[i].Cells["SeqNo"].Value != null)
                        tCase.seq[i].setSeqNo(Convert.ToInt16(SeqDataGridView.Rows[i].Cells["SeqNo"].Value).ToString("D4"));

                    if (SeqDataGridView.Rows[i].Cells["Desc"].Value != null)
                        tCase.seq[i].setDesc(SeqDataGridView.Rows[i].Cells["Desc"].Value.ToString());

                    if (SeqDataGridView.Rows[i].Cells["DiagCmd"].Value != null)
                        tCase.seq[i].setDiagCmd(SeqDataGridView.Rows[i].Cells["DiagCmd"].Value.ToString());

                    if (SeqDataGridView.Rows[i].Cells["Para"].Value != null)
                        tCase.seq[i].setPara(SeqDataGridView.Rows[i].Cells["Para"].Value.ToString());

                    if (SeqDataGridView.Rows[i].Cells["Exp"].Value != null)
                        tCase.seq[i].setExpected(SeqDataGridView.Rows[i].Cells["Exp"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to update the sequence number");
                return false;
            }

            return true;
        }

        private void createNewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (treeView1.Nodes["TestMenu"].Nodes.Count > 0 && treeView1.Nodes["TestMenu"].Nodes[0].Nodes.Count > 0)
                {
                    DialogResult result = MessageBox.Show("Would you like to export the XML now? Any unexported changes will be discarded.\n(Press cancel to abort)",
                                                          "Confirmation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                    if (result == DialogResult.No)
                    {
                        createNewXml();
                    }
                    else if (result == DialogResult.Yes)
                    {
                        createXml();
                        createNewXml();
                    }
                }
                else
                {
                    if (treeView1.Nodes["TestMenu"].Nodes.Count > 0)
                    {
                        DialogResult result = MessageBox.Show("Any deleted node cannot be undo. Are you sure?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (result == DialogResult.Yes)
                            createNewXml();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show("Error on creating a new XML"); }
        }

        private void createNewXml()
        {
            treeView1.Nodes.Clear();
            createTreeView();
            modules.Clear();
            tcCancel.Visible = false;
            gBoxSeq.Visible = false;
        }

        private void openSingleXml()
        {
            //Create a new instance of the OpenFileDialog
            OpenFileDialog dialog = new OpenFileDialog();

            //Set the file filter
            dialog.Filter = "XML files (*.xml)|*.xml";

            //Set Initial Directory
            dialog.InitialDirectory = Application.ExecutablePath;
            dialog.Title = "Select a xml file";

            //Present to the user. 
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                this.Cursor = Cursors.WaitCursor;
                // load data from xml to tree here
                fileDir = dialog.FileName;
                buildTreeViewFromXML();
                showHiddenControl();
                this.Cursor = Cursors.Default;
            }
        }

        private void openXmlInFolder()
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();

            if (folderDialog.ShowDialog() == DialogResult.OK)
            {
                this.Cursor = Cursors.WaitCursor;
                fileDir = folderDialog.SelectedPath;
                buildTreeViewFromXML();
                showHiddenControl();
                this.Cursor = Cursors.Default;
            }

            if (fileDir == String.Empty)
                return;//user didn't select a file
        }

        private void showHiddenControl()
        {
            treeView1.ExpandAll();
            tcCancel.Visible = true;
            gBoxSeq.Visible = true;
            disableTestCase();
        }

        private void buildTreeViewFromXML()
        {
            XmlDocument xmlDoc = new XmlDocument();
            string[] files;

            treeView1.BeginUpdate();
            treeView1.Nodes.Clear();
            createTreeView();
            modules.Clear();

            if (fileDir.Contains(".xml"))
                files = new string[1] { fileDir };
            else
                files = Directory.GetFiles(fileDir, "*.xml");

            for (int i = 0; i < files.Count(); i++)
            {
                try
                {

                    xmlDoc.Load(files[i]);
                }
                catch (NullReferenceException ne)
                {
                    showMsgBox("Please load XML file(s)", MessageBoxIcon.Error);
                    return;
                }
                catch (Exception ex)
                {
                    showMsgBox(Path.GetFileName(files[i]) + " format is incorrect!\n\n" + "Error: " + ex.Message,
                               MessageBoxIcon.Error);
                    return;
                }

                try
                {
                    XmlNode catNode = xmlDoc.GetElementsByTagName("Category")[0];
                    XmlNode modNode = xmlDoc.GetElementsByTagName("Module")[0];
                    if (!isNodeExists(catNode.InnerText, (int)NodeLevel.CATEGORY))
                        treeView1.Nodes["TestMenu"].Nodes.Add(createNormalTreeNode(catNode.InnerText));
                    if (!isNodeExists(modNode.InnerText, (int)NodeLevel.MODULE))
                        treeView1.Nodes["TestMenu"].Nodes[catNode.InnerText].Nodes.Add(createNormalTreeNode(modNode.InnerText));
                    modules.Add(new Module(catNode.InnerText, modNode.InnerText));

                    XmlNodeList testCaseList = xmlDoc.GetElementsByTagName("TestCase");
                    for (int j = 0; j < testCaseList.Count; j++)
                    {
                        treeView1.Nodes["TestMenu"].Nodes[catNode.InnerText].Nodes[modNode.InnerText].Nodes.Add(createNormalTreeNode(testCaseList[j].Attributes["tc"].InnerText));
                        modules[i].tc.Add(new TestCase(testCaseList[j].Attributes["tc"].InnerText,
                                                       testCaseList[j].SelectSingleNode("Desc").InnerText));

                        XmlNodeList seqNumList = testCaseList[j].SelectNodes("SeqNum");
                        foreach (XmlNode sn in seqNumList)
                        {
                            modules[i].tc[j].seq.Add(new TestSequence(sn.Attributes["sn"].InnerText,
                                                                  sn.SelectSingleNode("Desc").InnerText,
                                                                  sn.SelectSingleNode("DiagCmd").InnerText,
                                                                  sn.SelectSingleNode("Param").InnerText,
                                                                  sn.SelectSingleNode("Expect").InnerText));
                        }
                    }                    
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }

            //displaySeqNumInfo(modules[5].tc[0]);
            //displayModule(modules);
            treeView1.EndUpdate();
        }

        private bool isNodeExists(string key, int level)
        {
            try
            {
                TreeNode[] nodeArr = treeView1.Nodes.Find(key, true);
                for (int i = 0; i < nodeArr.Length; i++)
                {
                    if (String.Equals(nodeArr[i].Text, key) && nodeArr[i].Level == level)
                        return true;
                }
            }
            catch { }

            return false;
        }

        /*
         * For debugging purposes
         * To verify the value extract from xml
         */
        private void displayModule(List<Module> m)
        {
            string msg = string.Empty;

            for (int i = 0; i < m.Count(); i++)
            {
                msg += m[i].getModuleName() + Environment.NewLine;
            }

            MessageBox.Show(msg);
        }

        /*
         * For debugging purposes
         * To verify the value extract from xml and display in the description text box
         */
        private void displaySeqNumInfo(TestCase tCase)
        {
            string msg = string.Empty;

            msg += tCase.getTcNo() + Environment.NewLine;
            for (int i = 0; i < tCase.seq.Count; i++)
            {
                msg += "sn: " + tCase.seq[i].getSeqNo() + Environment.NewLine;
            }

            MessageBox.Show(msg);
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            displaySelectedNode();
        }

        private void displaySelectedNode()
        {
            int i = 0, index;
            TreeNode node = treeView1.SelectedNode;
            TreeNode parent = treeView1.SelectedNode.Parent;

            createContextMenuStrip();
            SeqDataGridView.Rows.Clear();
            testNo.Clear();
            testDesc.Clear();

            try
            {
                if (node.Level == (int)NodeLevel.TESTCASE)
                {
                    index = getModuleListIndex(parent.Text);

                    if (index == -1)
                        throw new Exception();

                    if (modules[index].tc.Count != parent.Nodes.Count)
                        return;

                    if (modules[index].tc[node.Index].getTcNo().Length > 3)
                        testNo.Text = modules[index].tc[node.Index].getTcNo().Substring(3, 4).TrimStart('0');  //trim prefix 'TC_'
                    testDesc.Text = modules[index].tc[node.Index].getDesc();

                    foreach (TestSequence sqNo in modules[index].tc[node.Index].seq)
                    {
                        SeqDataGridView.Rows.Add();
                        SeqDataGridView.Rows[i].Cells["SeqNo"].Value = sqNo.getSeqNo().TrimStart('0');
                        SeqDataGridView.Rows[i].Cells["Desc"].Value = sqNo.getDesc();
                        SeqDataGridView.Rows[i].Cells["DiagCmd"].Value = sqNo.getDiagCmd();
                        SeqDataGridView.Rows[i].Cells["Para"].Value = sqNo.getPara();
                        SeqDataGridView.Rows[i].Cells["Exp"].Value = sqNo.getExpected();
                        i++;
                    }
                }
            }
            catch (Exception ex) { reportInvalidIndex("Invalid index occur"); }
        }

        private void treeView1_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void treeView1_ItemDrag(object sender, ItemDragEventArgs e)
        {
            treeView1.DoDragDrop(e.Item, DragDropEffects.Move);
        }

        private void treeView1_DragOver(object sender, System.Windows.Forms.DragEventArgs e)
        {
            TreeNode NodeOver = this.treeView1.GetNodeAt(this.treeView1.PointToClient(Cursor.Position));
            TreeNode NodeMoving = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");

            if (NodeOver != null)
            {
                if (NodeOver.PrevVisibleNode != null)
                {
                    NodeOver.PrevVisibleNode.BackColor = Color.White;
                }
                if (NodeOver.NextVisibleNode != null)
                {
                    NodeOver.NextVisibleNode.BackColor = Color.White;
                }
                NodeOver.BackColor = Color.Aquamarine;
            }


            // A bit long, but to summarize, process the following code only if the nodeover is null
            // and either the nodeover is not the same thing as nodemoving UNLESSS nodeover happens
            // to be the last node in the branch (so we can allow drag & drop below a parent branch)
            if (NodeOver != null && (NodeOver != NodeMoving || (NodeOver.Parent != null && NodeOver.Index == (NodeOver.Parent.Nodes.Count - 1))))
            {
                int OffsetY = this.treeView1.PointToClient(Cursor.Position).Y - NodeOver.Bounds.Top;
                int NodeOverImageWidth = this.treeView1.ImageList.Images[(int)Images.NODE].Size.Width + 8;
                Graphics g = this.treeView1.CreateGraphics();

                if (OffsetY < (NodeOver.Bounds.Height / 2))
                {
                    #region If NodeOver is a child then cancel
                    TreeNode tnParadox = NodeOver;
                    while (tnParadox.Parent != null)
                    {
                        if (tnParadox.Parent == NodeMoving)
                        {
                            this.NodeMap = "";
                            return;
                        }

                        tnParadox = tnParadox.Parent;
                    }
                    #endregion
                    #region Store the placeholder info into a pipe delimited string
                    SetNewNodeMap(NodeOver, false);
                    if (SetMapsEqual() == true)
                        return;
                    #endregion
                    #region Clear placeholders above and below
                    this.Refresh();
                    #endregion
                    #region Draw the placeholders
                    this.DrawLeafTopPlaceholders(NodeOver);
                    #endregion
                }
                else
                {
                    #region If NodeOver is a child then cancel
                    TreeNode tnParadox = NodeOver;
                    while (tnParadox.Parent != null)
                    {
                        if (tnParadox.Parent == NodeMoving)
                        {
                            this.NodeMap = "";
                            return;
                        }

                        tnParadox = tnParadox.Parent;
                    }
                    #endregion
                    #region Allow drag drop to parent branches
                    TreeNode ParentDragDrop = null;
                    // If the node the mouse is over is the last node of the branch we should allow
                    // the ability to drop the "nodemoving" node BELOW the parent node
                    if (NodeOver.Parent != null && NodeOver.Index == (NodeOver.Parent.Nodes.Count - 1))
                    {
                        int XPos = this.treeView1.PointToClient(Cursor.Position).X;
                        if (XPos < NodeOver.Bounds.Left)
                        {
                            ParentDragDrop = NodeOver.Parent;

                            if (XPos < (ParentDragDrop.Bounds.Left - this.treeView1.ImageList.Images[ParentDragDrop.ImageIndex].Size.Width))
                            {
                                if (ParentDragDrop.Parent != null)
                                    ParentDragDrop = ParentDragDrop.Parent;
                            }
                        }
                    }
                    #endregion
                    #region Store the placeholder info into a pipe delimited string
                    // Since we are in a special case here, use the ParentDragDrop node as the current "nodeover"
                    SetNewNodeMap(ParentDragDrop != null ? ParentDragDrop : NodeOver, true);
                    if (SetMapsEqual() == true)
                        return;
                    #endregion
                    #region Clear placeholders above and below
                    this.Refresh();
                    #endregion
                    #region Draw the placeholders
                    DrawLeafBottomPlaceholders(NodeOver, ParentDragDrop);
                    #endregion
                }
            }
        }

        private void DrawLeafTopPlaceholders(TreeNode NodeOver)
        {
            Graphics g = this.treeView1.CreateGraphics();

            int NodeOverImageWidth = this.treeView1.ImageList.Images[(int)Images.NODE].Size.Width + 8;
            int LeftPos = NodeOver.Bounds.Left - NodeOverImageWidth;
            int RightPos = this.treeView1.Width - 4;

            Point[] LeftTriangle = new Point[5]{
												   new Point(LeftPos, NodeOver.Bounds.Top - 4),
												   new Point(LeftPos, NodeOver.Bounds.Top + 4),
												   new Point(LeftPos + 4, NodeOver.Bounds.Y),
												   new Point(LeftPos + 4, NodeOver.Bounds.Top - 1),
												   new Point(LeftPos, NodeOver.Bounds.Top - 5)};

            Point[] RightTriangle = new Point[5]{
													new Point(RightPos, NodeOver.Bounds.Top - 4),
													new Point(RightPos, NodeOver.Bounds.Top + 4),
													new Point(RightPos - 4, NodeOver.Bounds.Y),
													new Point(RightPos - 4, NodeOver.Bounds.Top - 1),
													new Point(RightPos, NodeOver.Bounds.Top - 5)};


            g.FillPolygon(System.Drawing.Brushes.Black, LeftTriangle);
            g.FillPolygon(System.Drawing.Brushes.Black, RightTriangle);
            g.DrawLine(new System.Drawing.Pen(Color.Black, 2), new Point(LeftPos, NodeOver.Bounds.Top), new Point(RightPos, NodeOver.Bounds.Top));

        }//eom

        private void DrawLeafBottomPlaceholders(TreeNode NodeOver, TreeNode ParentDragDrop)
        {
            Graphics g = this.treeView1.CreateGraphics();

            int NodeOverImageWidth = this.treeView1.ImageList.Images[(int)Images.NODE].Size.Width + 8;
            // Once again, we are not dragging to node over, draw the placeholder using the ParentDragDrop bounds
            int LeftPos, RightPos;
            if (ParentDragDrop != null)
                LeftPos = ParentDragDrop.Bounds.Left - (this.treeView1.ImageList.Images[ParentDragDrop.ImageIndex].Size.Width + 8);
            else
                LeftPos = NodeOver.Bounds.Left - NodeOverImageWidth;
            RightPos = this.treeView1.Width - 4;

            Point[] LeftTriangle = new Point[5]{
												   new Point(LeftPos, NodeOver.Bounds.Bottom - 4),
												   new Point(LeftPos, NodeOver.Bounds.Bottom + 4),
												   new Point(LeftPos + 4, NodeOver.Bounds.Bottom),
												   new Point(LeftPos + 4, NodeOver.Bounds.Bottom - 1),
												   new Point(LeftPos, NodeOver.Bounds.Bottom - 5)};

            Point[] RightTriangle = new Point[5]{
													new Point(RightPos, NodeOver.Bounds.Bottom - 4),
													new Point(RightPos, NodeOver.Bounds.Bottom + 4),
													new Point(RightPos - 4, NodeOver.Bounds.Bottom),
													new Point(RightPos - 4, NodeOver.Bounds.Bottom - 1),
													new Point(RightPos, NodeOver.Bounds.Bottom - 5)};


            g.FillPolygon(System.Drawing.Brushes.Black, LeftTriangle);
            g.FillPolygon(System.Drawing.Brushes.Black, RightTriangle);
            g.DrawLine(new System.Drawing.Pen(Color.Black, 2), new Point(LeftPos, NodeOver.Bounds.Bottom), new Point(RightPos, NodeOver.Bounds.Bottom));
        }//eom

        private void SetNewNodeMap(TreeNode tnNode, bool boolBelowNode)
        {
            NewNodeMap.Length = 0;

            if (boolBelowNode)
                NewNodeMap.Insert(0, (int)tnNode.Index + 1);
            else
                NewNodeMap.Insert(0, (int)tnNode.Index);
            TreeNode tnCurNode = tnNode;

            while (tnCurNode.Parent != null)
            {
                tnCurNode = tnCurNode.Parent;

                if (NewNodeMap.Length == 0 && boolBelowNode == true)
                {
                    NewNodeMap.Insert(0, (tnCurNode.Index + 1) + "|");
                }
                else
                {
                    NewNodeMap.Insert(0, tnCurNode.Index + "|");
                }
            }
        }//oem

        private bool SetMapsEqual()
        {
            if (this.NewNodeMap.ToString() == this.NodeMap)
                return true;
            else
            {
                this.NodeMap = this.NewNodeMap.ToString();
                return false;
            }
        }


        private void treeView1_DragDrop(object sender, DragEventArgs e)
        {
            TreeNode NewNode;

            if (e.Data.GetDataPresent("System.Windows.Forms.TreeNode", false))
            {
                Point pt = ((TreeView)sender).PointToClient(new Point(e.X, e.Y));
                TreeNode DestinationNode = ((TreeView)sender).GetNodeAt(pt);
                NewNode = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");

                if (DestinationNode != null)
                {
                    if (NewNode.Level == DestinationNode.Level && NewNode.Parent == DestinationNode.Parent)
                    {
                        handleNodeMoving(DestinationNode.Parent, NewNode.Index, DestinationNode.Index);
                    }
                    DestinationNode.BackColor = Color.White;
                    treeView1.SelectedNode = DestinationNode;
                }
                
            }
            this.Refresh();
        }

        private void handleNodeMoving(TreeNode parent, int fromIndex, int toIndex)
        {
            int status = toIndex - fromIndex;
            List<TreeNode> nodes;

            nodes = getNodeValues(parent);

            if (status > 0)
                nodes = swapDown(nodes, fromIndex, toIndex);
            else
                nodes = swapUp(nodes, fromIndex, toIndex);

            rearrangeTreeNodes(parent, nodes);
        }

        private void rearrangeTreeNodes(TreeNode parent, List<TreeNode> nodes)
        {
            int i = 0;
            removeAllChildNode(parent);
            foreach (TreeNode element in nodes)
            {
                parent.Nodes.Add(nodes[i]);
                i++;
            }
        }

        private void removeAllChildNode(TreeNode node)
        {
            if (node.Nodes.Count > 0)
            {
                for (int i = node.Nodes.Count - 1; i >= 0; i--)
                {
                    node.Nodes[i].Remove();
                }
            }
        }

        private List<TreeNode> getNodeValues(TreeNode parentNode)
        {
            List<TreeNode> nodes = new List<TreeNode>();
            foreach (TreeNode node in parentNode.Nodes)
            {
                nodes.Add(node);
            }
            return nodes;
        }

        private List<TreeNode> swapDown(List<TreeNode> nodes, int fromIndex, int toIndex)
        {
            TreeNode temp;
            TestCase tcTemp;
            int index = getModuleListIndex(treeView1.SelectedNode.Parent.Text);

            for (int i = fromIndex; i < toIndex; i++)
            {
                temp = nodes[i + 1];
                tcTemp = modules[index].tc[i + 1];
                nodes[i + 1] = nodes[i];
                modules[index].tc[i + 1] = modules[index].tc[i];
                nodes[i] = temp;
                modules[index].tc[i] = tcTemp;
            }
            return nodes;
        }

        private List<TreeNode> swapUp(List<TreeNode> nodes, int fromIndex, int toIndex)
        {
            TreeNode temp;
            TestCase tcTemp;
            int index = getModuleListIndex(treeView1.SelectedNode.Parent.Text);

            for (int i = fromIndex; i > toIndex; i--)
            {
                temp = nodes[i - 1];
                tcTemp = modules[index].tc[i - 1];
                nodes[i - 1] = nodes[i];
                modules[index].tc[i - 1] = modules[index].tc[i];
                nodes[i] = temp;
                modules[index].tc[i] = tcTemp;
            }
            return nodes;
        }

        private void SeqDataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SeqDataGridView.ClearSelection();
            SeqDataGridView.Rows[e.RowIndex].DefaultCellStyle.BackColor = SystemColors.Highlight;

            if(SeqDataGridView.ReadOnly == false)
            {
                if (e.Button == MouseButtons.Right)
                {
                    rowToDel = e.RowIndex;
                    deleteSeq.Show(Cursor.Position);
                }
            }
        }

        private void SeqDataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            for (int i = 0; i < SeqDataGridView.RowCount; i++)
            {
                SeqDataGridView.Rows[i].DefaultCellStyle.BackColor = DefaultBackColor;
            }
        }


        private void SeqDataGridView_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // If the mouse moves outside the rectangle, start the drag.
                if (dragBoxFromMouseDown != Rectangle.Empty &&
                !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    // Proceed with the drag and drop, passing in the list item.                    
                    DragDropEffects dropEffect = SeqDataGridView.DoDragDrop(
                          SeqDataGridView.Rows[rowIndexFromMouseDown],
                          DragDropEffects.Move);
                }
            }
        }

        private void SeqDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            // Get the index of the item the mouse is below.
            rowIndexFromMouseDown = SeqDataGridView.HitTest(e.X, e.Y).RowIndex;

            if (rowIndexFromMouseDown != -1)
            {
                // Remember the point where the mouse down occurred. 
                // The DragSize indicates the size that the mouse can move 
                // before a drag event should be started.                
                Size dragSize = SystemInformation.DragSize;

                // Create a rectangle using the DragSize, with the mouse position being
                // at the center of the rectangle.
                dragBoxFromMouseDown = new Rectangle(
                          new Point(
                            e.X - (dragSize.Width / 2),
                            e.Y - (dragSize.Height / 2)),
                      dragSize);
            }
            else
                // Reset the rectangle if the mouse is not over an item in the ListBox.
                dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void SeqDataGridView_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void SeqDataGridView_DragDrop(object sender, DragEventArgs e)
        {
            // The mouse locations are relative to the screen, so they must be 
            // converted to client coordinates.
            Point clientPoint = SeqDataGridView.PointToClient(new Point(e.X, e.Y));
            TreeNode node = treeView1.SelectedNode;

            // Get the row index of the item the mouse is below.
            rowIndexOfItemUnderMouseToDrop = SeqDataGridView.HitTest(clientPoint.X, clientPoint.Y).RowIndex;

            // If the drag operation was a move then remove and insert the row.
            if (e.Effect == DragDropEffects.Move)
            {
                DataGridViewRow rowToMove = e.Data.GetData(typeof(DataGridViewRow)) as DataGridViewRow;
                if ((rowIndexOfItemUnderMouseToDrop != SeqDataGridView.Rows.Count - 1) && (rowIndexFromMouseDown != SeqDataGridView.Rows.Count - 1))
                {
                    SeqDataGridView.Rows.RemoveAt(rowIndexFromMouseDown);
                    SeqDataGridView.Rows.Insert(rowIndexOfItemUnderMouseToDrop, rowToMove);
                }
            }
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                createXml();
            }
            catch (Exception ex) { MessageBox.Show("Unable to create XML"); }
        }

        private void createXml()
        {
            this.Cursor = Cursors.WaitCursor;
            XMLWriter writer = new XMLWriter();

            try
            {
                if (treeView1.Nodes["TestMenu"].Nodes.Count > 0 && modules.Count > 0)
                {
                    int index = writer.writeMultipleXML(modules, xmlPath);

                    if (index != -1)
                        showMsgBox("Error occured! Fail to create " + modules[index].getCategory() + "_" + modules[index].getModuleName() + ".xml", MessageBoxIcon.Error);
                    else
                        showMsgBox("Successfully generate XML file(s)", MessageBoxIcon.None);
                }
                else
                    showMsgBox("Please create at least one category and module before generate XML", MessageBoxIcon.Warning);
            }
            catch (Exception e) { MessageBox.Show(e.Message); }

            this.Cursor = Cursors.Default;
        }

        public void createXmlFolderPath()
        {
            appPath = Path.GetDirectoryName(Application.ExecutablePath);  // get the root path of the dir
            xmlPath = Path.Combine(appPath, "XML");                // get the path to the AppData folder

            if (!Directory.Exists(xmlPath))
            {
                Directory.CreateDirectory(xmlPath);
            }
        }

        private void openSingleXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (treeView1.Nodes["TestMenu"].Nodes.Count > 0 && treeView1.Nodes["TestMenu"].Nodes[0].Nodes.Count > 0)
                {
                    DialogResult result = MessageBox.Show("Would you like to export the XML first?\n(Press cancel to abort)",
                                                          "Confirmation", MessageBoxButtons.YesNoCancel);

                    if (result == DialogResult.No)
                    {
                        openSingleXml();
                    }
                    else if (result == DialogResult.Yes)
                    {
                        createXml();
                        openSingleXml();
                    }
                }
                else
                {
                    if (treeView1.Nodes["TestMenu"].Nodes.Count > 0)
                    {
                        DialogResult result = MessageBox.Show("Any deleted node cannot be undo. Are you sure?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (result == DialogResult.Yes)
                            openSingleXml();
                    }
                    else
                        openSingleXml();
                }
            }
            catch (Exception ex) { MessageBox.Show("Error on opening an XML file"); }
        }

        private void openXMLsInFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (treeView1.Nodes["TestMenu"].Nodes.Count > 0 && treeView1.Nodes["TestMenu"].Nodes[0].Nodes.Count > 0)
                {
                    DialogResult result = MessageBox.Show("Would you like to export the XML first?\n(Press cancel to abort)",
                                                          "Confirmation", MessageBoxButtons.YesNoCancel);

                    if (result == DialogResult.No)
                    {
                        openXmlInFolder();
                    }
                    else if (result == DialogResult.Yes)
                    {
                        createXml();
                        openXmlInFolder();
                    }
                }
                else
                {
                    if (treeView1.Nodes["TestMenu"].Nodes.Count > 0)
                    {
                        DialogResult result = MessageBox.Show("Any deleted node cannot be undo. Are you sure?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (result == DialogResult.Yes)
                            openXmlInFolder();
                    }
                    else
                        openXmlInFolder();
                }
            }
            catch (Exception ex) { MessageBox.Show("Error on opening XML files"); }
        }

        // test case cancel button
        private void button1_Click(object sender, EventArgs e)
        {
            if (selectionMode != (int)SelectionMode.EDIT)
            {
                TreeNode node = treeView1.SelectedNode.Parent;
                treeView1.SelectedNode.Remove();
                treeView1.SelectedNode = node;
            }
            else
            {
                testNo.Text = treeView1.SelectedNode.Text.Substring(3,4).TrimStart('0');
            }
                
            disableTestCase();
            displaySelectedNode();
        }

        private void xmlEditor_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
            {
                // Maximized!
                this.SeqNo.Width = (int)(30 * 1.7);
                this.Desc.Width = (int)(140 * 2.1 + 1);
                this.DiagCmd.Width = (int)(85 * 1.8);
                this.Para.Width = (int)(115 * 2.1);
                this.Exp.Width = (int)(115 * 2.1);
            }
            if (WindowState == FormWindowState.Normal)
            {
                // Restored!
                this.SeqNo.Width = 30;
                this.Desc.Width = 140;
                this.DiagCmd.Width = 85;
                this.Para.Width = 115;
                this.Exp.Width = 115;
            }

        }
    }
}
