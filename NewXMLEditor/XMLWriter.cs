﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;
using System.Windows.Forms;
using System.IO;

/*
      Copyright {2016} {Wong Yan Yin, Jackson Teh Ka Sing}

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
*/


namespace NewXMLEditor
{
    public class XMLWriter
    {
        public XMLWriter()
        {

        }

        public XElement[] writeTestCase(List<TestCase> testCases)
        {
            XElement[] tcNames = new XElement[testCases.Count()];
            int index = 0;

            foreach (TestCase testcase in testCases)
            {
                XElement tcName = new XElement("TestCase");
                XAttribute tc = new XAttribute("tc", testcase.getTcNo());
                XElement testDesc = new XElement("Desc", testcase.getDesc() );

                if (testcase.seq != null)
                {
                    XElement[] seqNo = writeSequenceNo(testcase.seq);

                    tcName.Add(tc);
                    tcName.Add(testDesc);
                    for (int i = 0; i < seqNo.Count(); i++)
                    {
                        tcName.Add(seqNo[i]);
                    }
                }

                tcNames[index] = tcName;
                index++;
            }

            return tcNames;
        }

        public XElement[] writeSequenceNo(List<TestSequence> sequences)
        {
            XElement[] seqNo = new XElement[sequences.Count()];
            XAttribute sn = null;
            XElement seqDesc = null;
            XElement diagcmd = null;
            XElement param = null;
            XElement expect = null;
            int i = 0;

            foreach(TestSequence sequence in sequences)
            {
                XElement sqNum = new XElement("SeqNum");

                if (!string.IsNullOrEmpty(sequence.getSeqNo()))
                    sn = new XAttribute("sn", sequence.getSeqNo().ToString());
                else
                    sn = new XAttribute("sn", string.Empty);

                if (!string.IsNullOrEmpty(sequence.getDesc()))
                    seqDesc = new XElement("Desc", sequence.getDesc().ToString());
                else
                    seqDesc = new XElement("Desc", string.Empty);

                if (!string.IsNullOrEmpty(sequence.getDiagCmd()))
                    diagcmd = new XElement("DiagCmd", sequence.getDiagCmd().ToString());
                else
                    diagcmd = new XElement("DiagCmd", string.Empty);

                if(!string.IsNullOrEmpty(sequence.getPara()))
                    param = new XElement("Param", sequence.getPara().ToString());
                else
                    param = new XElement("Param", string.Empty);

                if (!string.IsNullOrEmpty(sequence.getExpected()))
                    expect = new XElement("Expect", sequence.getExpected().ToString());
                else
                    expect = new XElement("Expect", string.Empty);

                sqNum.Add(sn);
                sqNum.Add(seqDesc);
                sqNum.Add(diagcmd);
                sqNum.Add(param);
                sqNum.Add(expect);
                seqNo[i] = sqNum;
                i++;
            }

            return seqNo;
        }

        public int writeToXML(string category, string module, List<TestCase> tc, string path)
        {
            try
            {
                XElement root = new XElement("TestMenu");
                XElement categoryName = new XElement("Category", category);
                XElement moduleName = new XElement("Module", module);
                XElement[] tCase;

                //add from root
                root.Add(categoryName);
                root.Add(moduleName);

                if (tc != null)
                {
                    tCase = writeTestCase(tc);

                    for (int i = 0; i < tCase.Count(); i++)
                        root.Add(tCase[i]);
                }

                // save changes
                root.Save(path);
                return 1;
            }
            catch(Exception ex)
            {
                Console.Write(ex.Message);
                return 0;
            }
        }


        public int writeMultipleXML(List<Module> modules, string xmlPath)
        {
            int status, i = 0;
            string path;

            try
            {
                foreach(Module mod in modules)
                {
                    path = Path.Combine(xmlPath, mod.getCategory() + "_" + mod.getModuleName() + ".xml");
                    status = writeToXML(mod.getCategory(), mod.getModuleName(), mod.tc, path);
                    if (status != 1)
                        throw new Exception(); ;
                    i++;
                }
                return -1;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return i;
            }  
        }
    }
}
